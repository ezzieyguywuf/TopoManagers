#include <SolidManagers.hpp>
#include <ModifiedSolids.hpp>

#include "gtest/gtest.h"
#include "gmock/gmock.h"

using ::testing::ElementsAreArray;

TEST(PrimitiveSolidManager, Create){
    Occ::Box myBox = Occ::Box(10, 10, 10);
    PrimitiveSolidManager mgr(myBox);
    uint i = mgr.getFaceIndex(myBox.getNamedFace(Occ::FaceName::top));
    EXPECT_GE(i, 0);
}

TEST(PrimitiveSolidManager, UpdateSolid){
    // Initialize manager and get consistent reference to front face
    Occ::Box box1 = Occ::Box(10, 10, 10);

    PrimitiveSolidManager mgr(box1);
    uint i = mgr.getFaceIndex(box1.getNamedFace(Occ::FaceName::front));

    // Create new box and update our manager
    Occ::Box box2 = Occ::Box(20, 90, 3);
    Occ::ModifiedSolid mod(box1, box2);
    mgr.updateSolid(mod);

    // retrieve the front face using our index. Also, get the expected face from the
    // created box2, to use in an assertion
    std::vector<Occ::Face> retrieved = mgr.getFaceByIndex(i);
    const Occ::Face& check = box2.getNamedFace(Occ::FaceName::front);

    EXPECT_THAT(retrieved, ElementsAreArray({check}));
}
