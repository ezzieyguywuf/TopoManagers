#include <Python.h>
#include <pybind11/pybind11.h>

#include "SolidManagersPy.hpp"

PYBIND11_MODULE(TopoManagers, m)
{
    m.doc() = R"pbdoc(
    TopoManagers
    ----------
    A collecion of managers for the topology of Solids.
    
    These managers will each own an Occ::Solid. Any time a sure attempts to update the
    Solid that this manager owns, it will require the user to provide sufficient information
    to provide consistent access to the various topological entities.

    In other words: by using a TopoManager, you can refer to a given Edge on a solid and
    have confidence that, despite changes to the underlying solid, your reference to that
    Edge will remain valid.
    )pbdoc";
    init_ISolidManagerPy(m);
    init_PrimitiveSolidManagerPy(m);
    init_CompoundSolidManagerPy(m);
}
