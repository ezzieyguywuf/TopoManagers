#include "SolidManagersPy.hpp"

ISolidManagerPyClass* init_ISolidManagerPy(py::module& m){
    ISolidManagerPyClass* myClass = new ISolidManagerPyClass(m, "ISolidManager");
    myClass->
        //def(py::init<>())
        def("getEdgeIndex", &ISolidManager::getEdgeIndex)
        .def("getEdgeByIndex", &ISolidManager::getEdgeByIndex)
        .def("getFaceIndex", &ISolidManager::getFaceIndex)
        .def("geFaceByIndex", &ISolidManager::getFaceByIndex)
        .def("translate", &ISolidManager::translate)
        .def("getSolid", &ISolidManager::getSolid);
    return myClass;
}

PrimitiveSolidManagerPyClass* init_PrimitiveSolidManagerPy(py::module& m)
{
    PrimitiveSolidManagerPyClass* primClass = new PrimitiveSolidManagerPyClass(m, "PrimitiveSolidManager"); 
    primClass->doc() = R"pbdoc(
        The Primitive Solid Manager will manage a single Occ::Solid in which any
        topological changes can be fully described using an Occ::ModifiedSolid.

        )pbdoc";
    primClass
        ->def(py::init([](Occ::Solid arg){
                    return PrimitiveSolidManager(arg);}))
        .def(py::init([](const PrimitiveSolidManager& aManager){
                    return PrimitiveSolidManager(aManager);}))
        .def("updateSolid", &PrimitiveSolidManager::updateSolid,
              py::arg("aModifiedSolid"),
              py::doc("Update the tracked solid."))
        .def("getEdgeIndex", &PrimitiveSolidManager::getEdgeIndex)
        .def("getEdgeByIndex", &PrimitiveSolidManager::getEdgeByIndex)
        .def("getFaceIndex", &PrimitiveSolidManager::getFaceIndex)
        .def("getFaceByIndex", &PrimitiveSolidManager::getFaceByIndex)
        .def("translate", &PrimitiveSolidManager::translate)
        .def("getSolid", &PrimitiveSolidManager::getSolid);
        ;
    return primClass;
}

CompoundSolidManagerPyClass* init_CompoundSolidManagerPy(py::module& m)
{
    CompoundSolidManagerPyClass* primClass = new CompoundSolidManagerPyClass(m, "CompoundSolidManager"); 
    primClass->doc() = R"pbdoc(
        The Primitive Solid Manager will manage a single Occ::Solid in which any
        topological changes can be fully described using an Occ::ModifiedSolid.

        )pbdoc";
    primClass
        ->def(py::init([](Occ::BooleanSolid arg){
                    return CompoundSolidManager(arg);}))
        .def(py::init([](const CompoundSolidManager& aManager){
                    return CompoundSolidManager(aManager);}))
        //.def("updateSolid", 
              //(void (CompoundSolidManager::*)(Occ::BooleanSolid)) &CompoundSolidManager::updateSolid,
              //py::arg("someSolids"),
              //py::doc("Update the tracked solid."))
        .def("updateSolid", 
              (void (CompoundSolidManager::*)(Occ::BooleanSolid, vector<Occ::ModifiedSolid>)) &CompoundSolidManager::updateSolid,
              py::arg("newSolid"),
              py::arg("modifiedBaseSolids"),
              py::doc("Update the tracked solid."))
        .def("getEdgeIndex", &CompoundSolidManager::getEdgeIndex)
        .def("getEdgeByIndex", &CompoundSolidManager::getEdgeByIndex)
        .def("getFaceIndex", &CompoundSolidManager::getFaceIndex)
        .def("getFaceByIndex", &CompoundSolidManager::getFaceByIndex)
        .def("translate", &CompoundSolidManager::translate)
        .def("getSolid", &CompoundSolidManager::getSolid);
        ;
    return primClass;
}

// -------- ISolidMangerPy Class Definition  ------------------
uint ISolidManagerPy::getFaceIndex(const Occ::Face& aFace) const{
    PYBIND11_OVERLOAD_PURE(
        unsigned int,  // Return type
        ISolidManager, // Parent class
        getFaceIndex,  // name of function in c++ (must match python name)
        aFace          // arguments
    );
}

vector<Occ::Face> ISolidManagerPy::getFaceByIndex(uint i) const{
    PYBIND11_OVERLOAD_PURE(
        std::vector<Occ::Face>,
        ISolidManager,
        getFaceByIndex,
        i
    );
}

void ISolidManagerPy::translate(double dx, double dy, double dz){
    PYBIND11_OVERLOAD_PURE(
        void,
        ISolidManager,
        translate,
        dx, dy, dz
    );
}

const Occ::Solid& ISolidManagerPy::getSolid() const{
    PYBIND11_OVERLOAD_PURE(
        const Occ::Solid&,
        ISolidManager,
        getSolid,
    );
}

ISolidManager* ISolidManagerPy::Clone_wrapper() const
{
    PYBIND11_OVERLOAD_PURE(
        ISolidManager*,  // Return type
        ISolidManager, // Parent class
        Clone,  // name of function in c++ (must match python name)
    );
}
