#pragma once

#include <Python.h>
#include "SolidManagers.hpp"
#include <pybind11/pybind11.h>
#include <pybind11/stl.h>

namespace py = pybind11;

class ISolidManagerPy;

using ISolidManagerPyClass = py::class_<ISolidManager, ISolidManagerPy>;
using PrimitiveSolidManagerPyClass = 
    py::class_<PrimitiveSolidManager, ISolidManager>;
using CompoundSolidManagerPyClass = 
    py::class_<CompoundSolidManager, ISolidManager>;

ISolidManagerPyClass* init_ISolidManagerPy(py::module&);
PrimitiveSolidManagerPyClass* init_PrimitiveSolidManagerPy(py::module&);
CompoundSolidManagerPyClass* init_CompoundSolidManagerPy(py::module&);

class ISolidManagerPy : public ISolidManager{
    //* A Trampoline class, as described in the pybind11 documentation.
    //* See https://pybind11.readthedocs.io/en/stable/advanced/classes.html#overriding-virtual-functions-in-python
    public:
        // Inherit constructors
        using ISolidManager::ISolidManager;

        // Trampoline: need one for every virtual function.
        uint getFaceIndex(const Occ::Face& aFace) const override;
        vector<Occ::Face> getFaceByIndex(uint i) const override;
        void translate(double dx, double dy, double dz) override;
        const Occ::Solid& getSolid() const override;
        // The PYBIND11_OVERLOAD_PURE macro will (I think) take care of the `override`
        // keyword
        ISolidManager* Clone_wrapper() const;
};
