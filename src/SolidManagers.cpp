#include <SolidManagers.hpp>

#include <utility>
#include <vector>
#include <iostream>
#include <stdexcept>
#include <string>
#include <algorithm> // for std::count

using std::vector;

ISolidManager::ISolidManager(const Occ::Solid& aSolid)
    : myFirstSolid(aSolid)
{}

ISolidManager::ISolidManager(const Occ::Solid& aSolid, const map<uint, pair<uint, uint>>& edges)
    : myFirstSolid(aSolid), mappedEdges(edges)
{}

uint ISolidManager::getEdgeIndex(const Occ::Edge& anEdge) const
{
    for (const auto& data : mappedEdges)
    {
        for (Occ::Edge checkEdge : this->getEdgeByIndex(data.first))
        {
            if (anEdge.isFlipped(checkEdge))
            {
                return data.first;
            }
        }
    }
    throw std::runtime_error("Was unable to find anEdge in mySolid.");
}

vector<Occ::Edge> ISolidManager::getEdgeByIndex(uint i) const
{
    auto faces1 = this->getFaceByIndex(mappedEdges.at(i).first);
    auto faces2 = this->getFaceByIndex(mappedEdges.at(i).second);

    vector<Occ::Edge> outEdges;
    for (const Occ::Face& face1 : faces1)
    {
        for (const Occ::Face& face2 : faces2)
        {
            if (face1.sharesEdge(face2))
            {
                outEdges.push_back(face1.getCommonEdge(face2));
            }
        }
    }
    if (outEdges.size() == 0)
    {
        throw std::runtime_error("That edge does not appear to be in mySolid");
    }
    return outEdges;
}

void ISolidManager::mapEdges()

{
    mappedEdges.clear();
    const Occ::Solid& theSolid = this->getSolid();
    uint count;
    for (const Occ::Edge& checkEdge : theSolid.getEdges())
    {
        count = 0;
        for (const Occ::Face& face1 : theSolid.getFaces())
        {
            bool found = false;
            if (not face1.containsEdge(checkEdge))
            {
                continue;
            }
            count++;
            for (const Occ::Face& face2 : theSolid.getFaces())
            {
                if (face1 == face2)
                {
                    continue;
                }
                if (face2.containsEdge(checkEdge))
                {
                    found = true;
                    count++;
                    uint i1 = this->getFaceIndex(face1);
                    uint i2 = this->getFaceIndex(face2);
                    mappedEdges.emplace(mappedEdges.size(), std::make_pair(i1, i2));
                    break;
                }
            }
            if (found)
            {
                break;
            }
        }

        if (count == 0)
        {
            throw std::runtime_error("Did not find two faces for this Edge");
        }
        else if (count == 1)
        {
            // TODO: do something with seam edges?
            std::clog << "Warning: Only one Edge found. Doing nothing. (is this a seam edge?)" << std::endl;
        }
    }
}

Occ::ModifiedSolid ISolidManager::createModifiedSolid(const ISolidManager& mgrOld,
                                                      const ISolidManager& mgrNew)
{
    if (mgrOld.myFirstSolid != mgrNew.myFirstSolid)
    {
        throw std::runtime_error("Both managers must share a myFirstSolid.");
    }

    // This will be constructed and then returned.
    Occ::ModifiedSolid modifiedSolid(mgrOld.getSolid(), mgrNew.getSolid());

    // First, add modified and deleted faces (we can check these at the same time)
    for (const Occ::Face& origFace : mgrOld.getSolid().getFaces())
    {
        // Since these managers are the same, the original index should work in the new
        // manager.
        uint i = mgrOld.getFaceIndex(origFace);
        bool isModified = false;
        for (const Occ::Face& modifiedFace : mgrNew.getFaceByIndex(i))
        {
            if (not isModified) isModified = true;
            modifiedSolid.addModified(origFace, modifiedFace);
        }
        if (not isModified)
        {
            modifiedSolid.addDeleted(origFace);
        }
    }

    // Next, add new faces
    for (const Occ::Face& checkFace : mgrNew.getSolid().getFaces())
    {
        //FIXME: ModifiedSolid::isModification is an 'expensive' operation, as it loops
        //       through all the Face in ModifiedSolid. This is an opportunity for
        //       optimization should you need it.
        if (not modifiedSolid.isModification(checkFace))
        {
            modifiedSolid.addGenerated(checkFace);
        }
    }

    return modifiedSolid;
}

//--------------------------------------------------------------------------
//                  CompoundSolidManager
//--------------------------------------------------------------------------
/** Used to manage indices to Occ::PrimitiveSolid
 *
 *  A "Primitive Solid" is one which can be defined fully by Occ::NamedFaces. In other
 *  words, it has a defined "Top", "Bottom", etc...
 */
PrimitiveSolidManager::PrimitiveSolidManager(Occ::Solid aSolid)
    : ISolidManager(aSolid), mySolid(aSolid)
{
    for (uint i = 0; i < mySolid.getFaces().size(); i++)
    {
        mappedFaces[mappedFaces.size()] = {i};
    }
    ISolidManager::mapEdges();
}

PrimitiveSolidManager::PrimitiveSolidManager(const PrimitiveSolidManager& aManager)
    : ISolidManager(aManager.myFirstSolid, aManager.mappedEdges), 
      mySolid(aManager.mySolid), 
      mappedFaces(aManager.mappedFaces)
{
    ISolidManager::mapEdges();
}

PrimitiveSolidManager PrimitiveSolidManager::operator=(const PrimitiveSolidManager& aManager)
{
    ISolidManager::myFirstSolid = aManager.myFirstSolid;
    this->mySolid = aManager.mySolid;
    this->mappedFaces = aManager.mappedFaces;
    ISolidManager::mapEdges();
    return *this;
}

uint PrimitiveSolidManager::getFaceIndex(const Occ::Face& aFace) const
{
    for (const auto& data : mappedFaces)
    {
        const vector<uint>& indices = data.second;
        //if (indices.size() > 1)
        //{
            //continue;
        //}
        if (aFace == mySolid.getFaces()[indices.at(0)])
        {
            return data.first;
        }
    }
    throw std::runtime_error("That face is not not in mySolid");
}

vector<Occ::Face> PrimitiveSolidManager::getFaceByIndex(uint i) const
{
    vector<Occ::Face> outFaces;
    for (uint index : mappedFaces.at(i))
    {
        outFaces.push_back(mySolid.getFaces()[index]);
    }
    return outFaces;
}

//bool PrimitiveSolidManager::hasEdge(uint i) const
//{
    //return mappedEdges.count(i);
//}

//bool PrimitiveSolidManager::hasFace(uint i) const
//{
    //return mappedFaces.count(i);
//}

void PrimitiveSolidManager::updateSolid(const Occ::ModifiedSolid& aModifiedSolid)
{
    if (mySolid != aModifiedSolid.getOrigSolid())
    {
        throw std::runtime_error("This ModifiedSolid does not appear to modify mySolid!");
    }

    for (auto& data : mappedFaces)
    {
        const uint& index = data.first;
        vector<uint>& faces = data.second;

        const Occ::Solid& origSolid = aModifiedSolid.getOrigSolid();
        Occ::Face origFace = origSolid.getFaces()[index];
        if (aModifiedSolid.isDeleted(origFace))
        {
            // when deleted, we point to an empty vector.
            faces = {};
        }
        else if (aModifiedSolid.isModified(origFace))
        {
            faces =  aModifiedSolid.getModifiedFace(origFace).get();
        }
    }

    for (uint i : aModifiedSolid.getGenerated())
    {
        mappedFaces[mappedFaces.size()].push_back(i);
    }

    mySolid = aModifiedSolid.getNewSolid();
}

void PrimitiveSolidManager::translate(double dx, double dy, double dz)
{
    mySolid.translate(dx, dy, dz);
}

const Occ::Solid& PrimitiveSolidManager::getSolid() const
{
    return mySolid;
}

std::unique_ptr<ISolidManager> PrimitiveSolidManager::Clone() const
{
    return std::unique_ptr<ISolidManager>(new PrimitiveSolidManager(*this));
}

//--------------------------------------------------------------------------
//                  CompoundSolidManager
//--------------------------------------------------------------------------

CompoundSolidManager::CompoundSolidManager(Occ::BooleanSolid aSolid)
    : ISolidManager(aSolid), myBoolSolid(aSolid)
{
    uint i = 0;
    for (const Occ::Face& newFace : myBoolSolid.getFaces())
    {
        Occ::BoolFaceRef ref = myBoolSolid.getFaceReference(newFace);
        mappedFaces.emplace(i, vector<Occ::BoolFaceRef>({ref}));
        i++;
    }
    ISolidManager::mapEdges();
}

CompoundSolidManager::CompoundSolidManager(const CompoundSolidManager& aManager)
    : ISolidManager(aManager.myFirstSolid, aManager.mappedEdges), 
      myBoolSolid(aManager.myBoolSolid), 
      mappedFaces(aManager.mappedFaces)
{
    ISolidManager::mapEdges();
}

CompoundSolidManager CompoundSolidManager::operator=(const CompoundSolidManager& aManager)
{
    ISolidManager::myFirstSolid = aManager.myFirstSolid;
    myBoolSolid = aManager.myBoolSolid;
    mappedFaces = aManager.mappedFaces;
    ISolidManager::mapEdges();
    return *this;
}

uint CompoundSolidManager::getFaceIndex(const Occ::Face& aFace) const
{
    for (const auto& data : mappedFaces)
    {
        uint index = data.first;
        for (const Occ::BoolFaceRef& ref : data.second)
        {
            const Occ::Face& checkFace = myBoolSolid.getBoolFace(ref);
            if (aFace.isFlipped(checkFace))
            {
                return index;
            }
        }
    }
    throw std::runtime_error("That face does not appear to originate from any of myBoolSolid.getModifiedSolids()");
}

vector<Occ::Face> CompoundSolidManager::getFaceByIndex(uint i) const
{
    // create return value vector - note that Occ::ModifiedSolid and Occ::BooleanSolid
    // return to us indices, which is why we have to build this return vector on our own.
    vector<Occ::Face> outFaces;

    for (const Occ::BoolFaceRef& aFaceRef : mappedFaces.at(i))
    {
        Occ::Face newFace = myBoolSolid.getBoolFace(aFaceRef);
        outFaces.push_back(newFace);
    }

    return outFaces;
}

const Occ::Solid& CompoundSolidManager::getSolid() const
{
    return myBoolSolid;
}

void CompoundSolidManager::translate(double dx, double dy, double dz)
{
    myBoolSolid.translate(dx, dy, dz);
}

/* Each modifiedBaseSolid will refer to one of the base Solid in the current
 * CompoundSolid.
 */
void CompoundSolidManager::updateSolid(Occ::BooleanSolid newSolid, 
                                       const vector<Occ::ModifiedSolid>& modifiedBaseSolids)
{
    // First, make sure these ModifiedSolids are correct
    unsigned int n = 0;
    for(const Occ::ModifiedSolid& mod : modifiedBaseSolids)
    {
        bool found = false;
        for(const Occ::ModifiedSolid& check : myBoolSolid.getModifiedSolids())
        {
            if(mod.getOrigSolid() == check.getOrigSolid())
            {
                found = true;
                n++;
                break;
            }
        }
        if(not found)
        {
            throw std::invalid_argument("Each ModifiedSolid __MUST__ correspond to an existing constituent Solid.");
        }
    }
    if(n != myBoolSolid.getModifiedSolids().size())
    {
        throw std::invalid_argument("Each constituent Solid __MUST__ be accounted for in the vector of ModifiedSolid passed to this function.");
    }
    map<unsigned int, vector<Occ::BoolFaceRef>> newMap;
    set<unsigned int> mappedNewFaces;
    for(auto& data : mappedFaces)
    {
        unsigned int i = data.first;
        for(const Occ::BoolFaceRef& ref : data.second)
        {
            const Occ::Face& origFace = myBoolSolid.getBaseFace(ref);
            for(const Occ::ModifiedSolid& mod : modifiedBaseSolids)
            {
                if(mod.getOrigSolid().hasFace(origFace))
                {
                    if(mod.isModified(origFace))
                    {
                        for(const Occ::Face& newOrigFace : mod.getModifications(origFace))
                        {
                            //FIXME: should I add a function to BooleanSolid that does
                            //this?
                            for(const Occ::ModifiedSolid& finalMod : newSolid.getModifiedSolids())
                            {
                                if(finalMod.getOrigSolid().hasFace(newOrigFace))
                                {
                                    if(finalMod.isModified(newOrigFace))
                                    {
                                        for(const Occ::Face& finalFace : finalMod.getModifications(newOrigFace))
                                        {
                                            Occ::BoolFaceRef newRef = newSolid.getFaceReference(finalFace);
                                            newMap[i].push_back(newRef);
                                            mappedNewFaces.insert(newSolid.getFaceIndex(finalFace));
                                        }
                                    }
                                    else
                                    {
                                        std::clog << "We found the finalMod, but this Face was not modified.....was it deleted?" << std::endl;
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        std::cerr << "Each face MUST be modified....or can it be deleted?" << std::endl;
                    }
                } // if origSolid.hasFace
            }// for -> modifiedBaseSolids
        } //for -> mappedFaces -> data.second (vector of BoolFaceRef)
    }//for -> mappedFaces
    for(unsigned int i = 0; i < newSolid.getFaces().size() ; i++)
    {
        if(mappedNewFaces.count(i) == 0)
        {
            std::cout << "index " << i << " in newSolid has not been accounted for - is this a new face?" << std::endl;
        }
    }
    mappedFaces = newMap;
    myBoolSolid = newSolid;
}

std::unique_ptr<ISolidManager> CompoundSolidManager::Clone() const
{
    return std::unique_ptr<ISolidManager>(new CompoundSolidManager(*this));
}
