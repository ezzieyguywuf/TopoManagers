- Determine why Occ::FeaturePartBoolean fails in FreeCAD when
  Topo::ISolidManager::makeModifiedSolid is called (throws runtime_error due to
  mgrOld.MyFirstSolid != mgrNew.MyFirstSolid)
