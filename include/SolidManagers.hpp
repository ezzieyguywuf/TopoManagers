#pragma once

#include <Shapes.hpp>
#include <PrimitiveSolids.hpp>
#include <ModifiedSolids.hpp>

#include <map>
#include <utility>
#include <vector>
#include <set>
#include <array>
#include <stdexcept>

using std::map;
using std::pair;
using std::vector;
using std::set;
using std::array;
using Occ::ModifiedSolid;

class ISolidManager
{
    public:
        ISolidManager() = default;
        // We explicitly delete these two so that pybind11 can recognize that it's not
        // possible to copy this.
        ISolidManager(const ISolidManager& aManager) = delete;
        ISolidManager& operator=(const ISolidManager& aManager) = delete;

        ISolidManager(const Occ::Solid& aSolid);
        ISolidManager(const Occ::Solid& aSolid, const map<uint, pair<uint, uint>>& edges);
        virtual ~ISolidManager(){};

        // returns an index that can  be used to consistently retrieve a topological Edge
        uint getEdgeIndex(const Occ::Edge& anEdge) const;
        // consistently returns the topological edge refered to by i
        vector<Occ::Edge> getEdgeByIndex(uint i) const;
        // returns an index that can be used to consistently retrieve a topological Face
        virtual uint getFaceIndex(const Occ::Face& aFace) const = 0;
        // consistently returns the topological face refered to by i
        virtual vector<Occ::Face> getFaceByIndex(uint i) const = 0;
        // apply a translation to the underlying solid. 
        // TODO, there is quite a bit of boilerplate with this, is there a way to reduce
        // that?
        virtual void translate(double dx, double dy, double dz) = 0;
        virtual const Occ::Solid& getSolid() const = 0;

        // Creates a Occ::ModifiedSolid that explains the differences between
        // mgrOld.mySolid and mgrNew.mySolid
        //
        // throws std::runtime_error if mgrNew.myFirstSolid and mgrOld.myFirstSolid are not
        // equivalent.
        static ModifiedSolid createModifiedSolid(const ISolidManager& mgrOld,
                                                 const ISolidManager& mgrNew);

        // Create a copy of the ISolidyManager
        virtual std::unique_ptr<ISolidManager> Clone() const = 0;

    protected:
        // Note: this must be called by the inheriting class after initializing whatever
        // face mapping they need to do. mapEdges makes use of getFaceIndex
        void mapEdges();
        // Used to check validity of parameter in getModifiedSolid
        Occ::Solid myFirstSolid;
        // A map in which the key will always refer to the same edge in
        // getSolid().getEdges(). This is done by identifying each edge by the two faces that
        // make it up. Therefore, in order to identify an edge, we simply keep track of
        // each face.
        map<uint, pair<uint, uint>> mappedEdges;
};

class PrimitiveSolidManager : public ISolidManager
{
    public:
        PrimitiveSolidManager() = default;
        PrimitiveSolidManager(Occ::Solid aSolid);

        // rule of three
        PrimitiveSolidManager(const PrimitiveSolidManager& aManager);
        PrimitiveSolidManager operator=(const PrimitiveSolidManager& aManager);
        ~PrimitiveSolidManager() = default;

        // Override methods from ISolidManager
        uint getFaceIndex(const Occ::Face& aFace) const override;
        vector<Occ::Face> getFaceByIndex(uint i) const override;
        void translate(double dx, double dy, double dz) override;
        const Occ::Solid& getSolid() const override;
        std::unique_ptr<ISolidManager> Clone() const override;

        // Note: need to extend to deal with deleted and generated faces. Right now only
        // deals with modified faces.
        void updateSolid(const Occ::ModifiedSolid& aModifiedSolid);


    private:
        Occ::Solid mySolid;
        // a map in which the key (which is unique) can be used to consistently refer to a
        // topological face in mySolid. The value associated with each key points to a
        // vector of indices. These indices  represent the actual mySolid.getFaces() that
        // the key refers to. We store a vector of indices rather than a single index
        // because a given face could be split - for example, let's say we start with a
        // 4-sided pyramid and decide to "transform" it into a 6-sided cube. The user may
        // wish to describe the operation is splitting two of the existing pyramid faces.
        map<uint, vector<uint>> mappedFaces;
};

class CompoundSolidManager : public ISolidManager
{
    public:
        CompoundSolidManager(Occ::BooleanSolid someSolids);

        //rule of three
        CompoundSolidManager(const CompoundSolidManager& aManager);
        CompoundSolidManager operator=(const CompoundSolidManager& aManager);
        ~CompoundSolidManager() = default;

        uint getFaceIndex(const Occ::Face& aFace) const override;
        vector<Occ::Face> getFaceByIndex(uint i) const override;
        const Occ::Solid& getSolid() const override;
        void translate(double dx, double dy, double dz) override;
        std::unique_ptr<ISolidManager> Clone() const override;

        /** When we update our solid, we need two things:
         *      1) The new Occ::BooleanSolid
         *      2) A list of Occ::ModifiedSolid, one for each base solid in the original
         *         Occ::BooleanSolid (i.e. myBoolSolid)
         *
         *  NOTE: Even if the base Occ::Solid has not changed, we still need an
         *  Occ::ModifiedSolid for it - std::runtime error will be thrown otherwise.
         *  Here's why.
         *
         *  Take for example a simple cube and cylinder that have been fused. Our original
         *  myBoolSolid may consist of a cube with edge length of 10 and a cylinder of
         *  radius 2.5 and height 10.
         *
         *  When updateSolid is called, the new fused shape has the same cube but a
         *  cylinder of height 5. One would expect that since only the cylinder has
         *  changed, one Occ::ModifiedSolid can be provided. However, due to some
         *  internals in OpenCasCade, the mapping between the (unchanged) faces on the
         *  Cube to those in the resultant fused Solid have probably changed - it is for
         *  this reason that an Occ::ModifiedSolid is needed for all base shapes, even
         *  those that have not geometrically changed.
         *
         *  \throws std::runtime_error if all base Occ::Shape are not accounted for in
         *          \p modifiedBaseSolids
         */
        void updateSolid(Occ::BooleanSolid newSolid, 
                         const vector<Occ::ModifiedSolid>& modifiedBaseSolids);
    private:
        // If a new consttuent Solid was added to our BooleanSolid, we need to account for
        // it.
        // TODO: Implement this
        //void addNewBaseSolid(const Occ::ModifiedSolid& newModSolid);
        Occ::BooleanSolid myBoolSolid;

        // Please see the documentation for Occ::BoolFaceRef for more information - in
        // short, Occ::BoolFaceRef contains the data necessary to trace back the origins
        // of a given face in an Occ::BooleanSolid. Just like in PrimitiveSolidManager, we
        // must account for the possibility of a Face getting split, thus we store a
        // vector of Occ::BoolFaceRef.
        map<uint, vector<Occ::BoolFaceRef>> mappedFaces;
};
